import socket

target_host = "www.google.dk"
target_port = 80

# create a socket object
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# connect the client
client.connect((target_host, target_port))

# the following will print the IP and port used on the client
print("The socket is using IP: {}, port: {}\n\r".format(*client.getsockname()))


# send some data
# for Python 3 we need to encode the string to, e.g. UTF-8
message = "GET / HTTP/1.1\r\nHost: google.dk\r\n\r\n"
client.send(message.encode('utf-8'))

# receive some data 
response = client.recv(4096)

print("Response from server:\n\r")
print(response.decode('utf-8'))