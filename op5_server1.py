import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('127.0.0.1', 8085))
sock.listen(5)
while True:
    print("Listening for new connection")
    conn,addr = sock.accept()
    print("Connection from "+str(addr))
    header = conn.recv(1000).decode("utf-8")
    headerlines = header.split("\r\n")
    print(headerlines[0])

    result = ""
    with open("response.txt","r") as f:
      result = f.read();

    conn.send("HTTP/1.1 200 OK\n".encode("utf-8"));
    conn.send("Server: fup og fidus\n".encode("utf-8"));
    conn.send("Last-Modified: Wed, 25 Jul 2018 19:15:56 GMT\n".encode("utf-8"));
    conn.send("Content-Type: text/html; charset=utf-8\n".encode("utf-8"));
    conn.send(("Content-Length: "+str(len(result))+"\n").encode("utf-8"));
    conn.send("Connection: Close\n".encode("utf-8"));
    conn.send("\n".encode("utf-8"));


    conn.send(result.encode("utf-8"));
    conn.close()

