import socket
from datetime import datetime

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('127.0.0.1', 9992))
sock.listen(5)


def chat_bot(message):
    if message == u'hej':
        output = "hej\n"
    elif message == u'klokken':
        now = datetime.now()
        output = "The time is: {}\n".format(now.strftime("%H:%M"))
    elif message == u'øl':
        output = "ja tak\n"
    else:
        output = "No more øl tilbage\n"
    return output


while True:
    print("Listening for new connection")
    conn,addr = sock.accept()
    print("Connection from "+str(addr))
    line = conn.recv(1024).decode("utf-8").strip()
    conn.send(chat_bot(line).encode("utf-8"))
    conn.close()