import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('127.0.0.1', 9992))
sock.listen(5)
while True:
    print("Listening for new connection")
    conn,addr = sock.accept()
    print("Connection from "+str(addr))
    conn.send("Hello World\n".encode("utf-8"))
    conn.close()
