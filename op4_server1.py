import socket
import sys
from datetime import datetime

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('192.168.1.186', 9992))
sock.listen(5)


def chat_bot(message):
    if message == u'hej':
        output = "hej tilbage\n"
    elif message == u'klokken':
        now = datetime.now()
        output = "The time is: {}\n".format(now.strftime("%H:%M"))
    elif message == u'øl':
        output = "ja tak\n"
    elif message == u'ol':
        output = "ja tak\n"
    elif message == u'farvel':
        print("Goodbye - Server Disconnected")
        conn.send("Goodbye - Client Disconnected (Message Sent From Server)".encode("utf-8"))
        sys.exit()
    else:
        output = "Invalid Input, Try Again !!!\n"
    return output


while True:
    print("Listening for new connection")
    conn,addr = sock.accept()
    print("----------------------------------------------------")
    print("Connection established from " + str(addr))
    conn.send("Established Connection (Message Sent From Server)".encode("utf-8"))
    while True:
        line = conn.recv(1024).decode("utf-8").strip()
        conn.send(chat_bot(line).encode("utf-8"))