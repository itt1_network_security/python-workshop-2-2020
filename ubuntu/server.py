import sys
import socket
import subprocess


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('localhost', 9992))
sock.listen(5)


def chat_bot(message):
    if message == u'hej':
        output = "hej tilbage\n"
    elif message == u'shadow':
        list_files = subprocess.run(["ls", "-l"])
        output = "The exit code was: %d" % list_files.returncode
    elif message == u'farvel':
        print("Goodbye - Server Disconnected")
        conn.send("Goodbye - Client Disconnected (Message Sent From Server)".encode("utf-8"))
        sys.exit()
    else:
        output = "Invalid Input, Try Again !!!\n"
    return output


while True:
    print("Listening for new connection")
    conn,addr = sock.accept()
    print("----------------------------------------------------")
    print("Connection established from " + str(addr))
    conn.send("Established Connection (Message Sent From Server)".encode("utf-8"))
    while True:
        line = conn.recv(1024).decode("utf-8").strip()
        conn.send(chat_bot(line).encode("utf-8"))