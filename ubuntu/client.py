import sys
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ("localhost", 9992)
print("Connecting to " + str(server_address))
sock.connect(server_address)
message = ""

while True:
    data = sock.recv(1024)
    print("Received\n" + data.decode("utf-8"))
    print("--------------------------------------------------------------------")
    print("Type 'shadow' or 'passwd' /// Type 'farvel' to stop program: ")
    message = sys.stdin.readline()
    if message != "farvel":
        sock.send(message.encode("utf-8"))
    else:
        sock.connect(server_address)
